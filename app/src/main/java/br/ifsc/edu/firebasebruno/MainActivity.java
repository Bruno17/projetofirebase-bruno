package br.ifsc.edu.firebasebruno;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {
    FirebaseAuth mAuth;
    EditText editTextLogin, editTextSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        //Create new account
        //mAuth.createUserWithEmailAndPassword("bruno@ifsc.edu.br","555senha");
        //mAuth.signOut();

        //mAuth.signInWithEmailAndPassword("bruno@ifsc.edu.br","555senha").addOnCompleteListener(new OnCompleteListener<AuthResult>(){
          //  @Override
            //public void onComplete(@NonNull Task < AuthResult > task) {
                //if (task.isSuccessful()) {
                  //  Toast.makeText(getApplicationContext(), mAuth.getCurrentUser().getEmail(), Toast.LENGTH_LONG).show();
                  //  Intent i = new Intent(getApplicationContext(), PrincipalActivity.class);
                  //  startActivity(i);
            //} else {
              //  Toast.makeText(getApplicationContext(), "Falha no Login", Toast.LENGTH_LONG).show();
           // }
       // }
        //});
        }

    public void login(View view){
        final String login = editTextLogin.getText().toString();
        String senha = editTextSenha.getText().toString();
        mAuth.signOut();

        if (!login.trim().equals("")) {
            mAuth.signInWithEmailAndPassword(login, senha).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()){
                        Toast.makeText(getApplicationContext(),"Sucesso!"+mAuth.getCurrentUser().getEmail(), Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(getApplicationContext(),"Falhou!"+login, Toast.LENGTH_LONG).show();
                    }
                }
            });

            FirebaseUser firebaseUser = mAuth.getCurrentUser();

            if(firebaseUser!=null){
                Intent intent = new Intent(getApplicationContext(), PrincipalActivity.class);
                startActivity(intent);
            }
        }
    }

    public void cadastrar(View view) {
        Intent i = new Intent(getApplicationContext(), CadastrarUsuarioActivity.class);
        startActivity(i);
    }

    public void RecuperaSenha(View view) {
        if (editTextLogin.getText().toString().trim().equals("")) {
            mAuth.sendPasswordResetEmail(editTextLogin.getText().toString());
        }
    }
}

