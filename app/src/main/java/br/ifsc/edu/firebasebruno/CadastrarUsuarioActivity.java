package br.ifsc.edu.firebasebruno;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class CadastrarUsuarioActivity extends AppCompatActivity {

    FirebaseAuth firebaseAuth;
    EditText editTextLogin, editTextSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_usuario);
        firebaseAuth = firebaseAuth.getInstance();

        editTextLogin = findViewById(R.id.editTextName);
        editTextSenha = findViewById(R.id.editTextSenha);

    }

    public void cadastrarNovoUsuario(View view) {
        if(editTextLogin.getText().toString().trim().equals("")){
            firebaseAuth.createUserWithEmailAndPassword(
                    editTextLogin.getText().toString(),
                    editTextSenha.getText().toString()
            ).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()){
                        Toast.makeText(getApplicationContext(),"Sucesso!"+firebaseAuth.getCurrentUser().getEmail(), Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(getApplicationContext(),"Falhou!", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }



    }
}
